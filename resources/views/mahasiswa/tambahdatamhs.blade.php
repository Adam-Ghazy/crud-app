@extends('layout.main')

@section('content')
    <h3>Form Tambah Data Mahasiswa</h3>
    <div class="card">
        <div class="card-header">
            <button type="button" class="btn btn-sm btn-warning" onclick="window.location='{{ url('mahasiswa') }}'">
                <i class=""></i> Kembali
            </button>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ url('mahasiswa') }}">
                @csrf
                <div class="row mb-3">
                    <label for="txtid" class="col-sm-2 col-form-label">NIM Mahasiswa</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm @error('txtid') is-invalid @enderror"
                            id="txtid" name="txtid">
                        @error('txtid')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="txtname" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control form-control-sm @error('txtname') is-invalid @enderror"
                            id="txtname" name="txtname">
                        @error('txtname')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="txtkelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-4">
                        <select class="form-select form-select-sm @error('txtkelamin') is-invalid @enderror" name="txtkelamin"
                            id="txtkelamin">
                            <option value="" selected>-Pilih-</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                        @error('txtkelamin')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="txtalamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea class="form-control @error('txtalamat') is-invalid @enderror" name="txtalamat" id="txtalamat" cols="30"
                            rows="10"></textarea>
                        @error('txtalamat')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="txtemail" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control form-control-sm  @error('txtemail') is-invalid @enderror"
                            id="txtemail" name="txtemail">
                        @error('txtemail')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="txtphone" class="col-sm-2 col-form-label">Nomor Telp</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control form-control-sm @error('txtphone') is-invalid @enderror"
                            id="txtphone" name="txtphone">
                        @error('txtphone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-sm btn-success">
                            Simpan
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
