@extends('layout.main')

@section('content')
    <h3>Data Mahasiswa</h3>
    <div class="card">
        <div class="card-header">
            <button type="button" class="btn btn-sm btn-primary" onclick="window.location='{{ url('mahasiswa/tambahdata') }}'">
                <i class="fas fa-plus-square"></i> Add New Data
            </button>
        </div>
        <div class="card-body">
            <table class="table table-sm table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Id Mahasiswa</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Alamat</th>
                        <th>Email</th>
                        <th>Nomor tlp</th>
                        <th>#</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($mahasiswa as $row)
                        <tr>
                            <th>{{ $loop->iteration }}</th>
                            <td>{{ $row->idmahasiswa }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ ($row->kelamin=='M') ? 'Male': 'Famale' }}</td>
                            <td>{{ $row->alamat }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->phoneNumber }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
